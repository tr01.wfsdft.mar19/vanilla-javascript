var cosas = [
    "Reloj de arena decorativo TILLSYN,descripción del objeto,https://www.ikea.com/es/es/images/range-introduction/ikea-ikea-tillsyn-hourglass-begåvning-glass-dome__1364493362974-s31.jpg,10",
    "LATTJO Peluche,descripción del objeto,https://www.ikea.com/es/es/images/products/lattjo-peluche-robot-verde-claro__0520262_pe642048_s4.jpg,12",
    "DRAGON Cubertería 24 piezas,descripción del objeto,https://www.ikea.com/es/es/images/products/dragon-cubertería-24-piezas-ac-inox__20609_pe105762_s4.jpg,22"
];

// var item = document.cloneNode('#item');
// //console.log(item.body.childNodes[3].children[0]);
// var divContainer = item.body.childNodes[3].children[0];
// //console.log(divContainer.childNodes[1]);
// if('content' in divContainer.childNodes[1]){
//     var templateContent = divContainer.childNodes[1].content;
//     console.log(templateContent.childNodes[1]);
//     document.body.appendChild(templateContent.cloneNode(true));

//     var templateChildren = templateContent.childNodes[1].children;
//     console.log(templateChildren);
//     var img = templateContent.childNodes[1].children[0];
//     var div = templateContent.childNodes[1].children[1];
    
// }


var Ikea = function(cosas){
    this.cosas = cosas;
    this.item = null;
    this.itemCantidad = 0;
    this.itemStock = 10;
    this.stock = new Array();
    this.shoppingChart = new Array();
    this.subtotal = 0.0;
    
    this.crearStock = function(){

        for(var i=0; i<this.cosas.length; i++){
            this.item = this.cosas[i].split(',');
            this.stock.push({item: this.item[0], description: this.item[1], url: this.item[2], price: this.item[3], stock: this.itemStock, cantidad: this.itemCantidad});

            var templ = document.querySelector('#item'); // selecciono el template
            var clon = templ.content.cloneNode(true); // clono el template y lo almaceno en clon
            var img = clon.querySelector('#img'); // a partir de aquí modifico el clon
            img.src = this.item[2];
            var name = clon.querySelector('#nombre');
            name.textContent = this.item[0];
            var desc = clon.querySelector('#desc');
            desc.textContent = this.item[1];
            var price = clon.querySelector('#precio');
            price.textContent = this.item[3] + ' €';
            var stock = clon.querySelector('#stock');
            stock.innerText = 'stock: ' + this.itemStock;

            clon.querySelector('.btn').addEventListener('click', (e)=>{
                
                //console.log(bought);
                var name = e.target.parentNode.childNodes[1].innerText;
                // var desc = bought[3].innerText;
                // var price = bought[5].innerText;
                // var stock = bought[7].innerText;
                this.comprarObjeto({item: name});
            });
            document.body.querySelector('#colItems').appendChild(clon); // añadimos el clon el body
        }
    };

    this.refreshShoppingChart = function(){
        var cestaDOM = document.querySelector('#cesta');
        cestaDOM.innerHTML ="";
    };

    this.createShoppingChart = function(){

        this.refreshShoppingChart();

        var templ = document.querySelector('#itemsCesta');
        console.log(this.getShoppingChart());
        document.querySelector('#cesta').classList.remove('invisible');

        this.getShoppingChart().forEach(item=>{
            var clon = templ.content.cloneNode(true);
            var nombre = clon.querySelector('.nombre');
            var precio = clon.querySelector('.precio');
            var cantidad = clon.querySelector('.cantidad');
            var subtotal = clon.querySelector('.subtotal');
            nombre.textContent = item.item;
            precio.textContent = item.price + ' €';
            cantidad.textContent = item.cantidad;
            subtotal.textContent = 'Subtotal: ' + parseFloat(this.subtotal);
            document.body.querySelector('#cesta').appendChild(clon);
        });
    };

    this.getStock = function(){
        return this.stock;
    };

    this.findItem = function(item){
    
        var foundItem = this.stock.find((elem)=>{
            return elem.item == item.item;
        });
        return foundItem;
    };

    this.findItemInShoppingChart = function(item){
        var foundItem = this.getShoppingChart().find((elem)=>{
            return elem.item == item.item
        });
        return foundItem;
    };

    this.comprarObjeto = function(item){
        
        if(this.findItem(item)){
            this.item = this.findItem(item);
            if(this.item.stock > 0){
                this.item.stock -= 1;
                if(this.findItemInShoppingChart(item)){
                    this.item.cantidad += 1;
                }else{
                    this.item.cantidad += 1;
                    this.shoppingChart.push(this.item);
                }
                this.subtotal += parseFloat(this.item.price);
                this.createShoppingChart();
                return this.shoppingChart;
            }else{
                return new String('Objeto no disponible');
            }
        }
    };

    this.getShoppingChart = function(){
        
        return this.shoppingChart;
    };
};


//miTienda.findItem({item: "Reloj de arena decorativo	TILLSYN", description: "descripcion del objeto", url: "https://www.ikea.com/es/es/images/range-introduction/ikea-ikea-tillsyn-hourglass-begåvning-glass-dome__1364493362974-s31.jpg", stock: "10"});

function init(){

    var miTienda = new Ikea(cosas);
    miTienda.crearStock();
    // miTienda.createShoppingChart();
    //console.log('find Reloj de arena decorativo	TILLSYN : ', miTienda.findItem({item: "Reloj de arena decorativo	TILLSYN"}));
    // miTienda.comprarObjeto({item: "Reloj de arena decorativo	TILLSYN"});
    // miTienda.comprarObjeto({item: "DRAGON  Cubertería 24 piezas"});
    // console.log('shopping chart: ', miTienda.showShoppingChart());
    // console.log('stock: ', miTienda.getStock());

    /*
    miTienda.comprarObjeto({item: "Reloj de arena decorativo	TILLSYN"});
    miTienda.comprarObjeto({item: "Reloj de arena decorativo	TILLSYN"});
    miTienda.comprarObjeto({item: "Reloj de arena decorativo	TILLSYN"});
    miTienda.comprarObjeto({item: "Reloj de arena decorativo	TILLSYN"});
    miTienda.comprarObjeto({item: "Reloj de arena decorativo	TILLSYN"});
    miTienda.comprarObjeto({item: "Reloj de arena decorativo	TILLSYN"});
    miTienda.comprarObjeto({item: "Reloj de arena decorativo	TILLSYN"});
    miTienda.comprarObjeto({item: "Reloj de arena decorativo	TILLSYN"});
    miTienda.comprarObjeto({item: "Reloj de arena decorativo	TILLSYN"});
    miTienda.comprarObjeto({item: "Reloj de arena decorativo	TILLSYN"}); // error, no hay stock
    */


};

init();
